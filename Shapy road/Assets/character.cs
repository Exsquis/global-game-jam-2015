﻿using UnityEngine;
using System.Collections;

public class character : MonoBehaviour {
	public float normalSpeed;
	public float maxSpeed;

	public float limitSpeed;

	public int shapeId;

	public float smooth;

	public Quaternion rotateZero;
	public GameObject[] pattern;

	public Vector3 challengePosition;
	public float popDistance;

	public GameObject[] Target;
	public int targetState;

	public float Speed;

	public float XSpeed;
	public float YSpeed;
	public float ZSpeed;

	public float timeToReact;
	public float timeToReactBase;
	public float timeToReactMalus;
	private float __counter;


	private CharacterController __controller;

	public GameObject challengeObject;
	public challenge challengeScript;
	

	public bool choice;
	public int input;

	public GameObject model;

	public Mesh[] Shape;
	// Use this for initialization
	void Start () {


		this.timeToReact = this.timeToReactBase;
		this.Speed = this.normalSpeed;

		this.challengeScript = this.challengeObject.GetComponent<challenge> ();
		this.choice = false;
		this.shapeId = Random.Range (0, this.Shape.Length - 1);
		this.model.GetComponent<MeshFilter>().mesh = this.Shape [this.shapeId];
		//this.Target[0] = this.challengeScript.target;
		this.challengePosition = new Vector3 (this.gameObject.transform.localPosition.x, this.gameObject.transform.localPosition.y, this.gameObject.transform.localPosition.z + popDistance);

	}
	
	// Update is called once per frame
	void Update () {



		var locVel = transform.InverseTransformDirection(rigidbody.velocity);
		locVel.x = this.XSpeed;
		locVel.y = this.YSpeed;
		locVel.z = this.ZSpeed;
		//rigidbody.velocity = transform.TransformDirection(locVel);
		rigidbody.velocity = transform.TransformDirection(Vector3.forward) * Speed * Time.deltaTime;

		//this.gameObject.transform.LookAt (this.Target[this.targetState].transform.position);
		var targetRotation = Quaternion.LookRotation(Target[this.targetState].transform.position - transform.position);
		
		// Smoothly rotate towards the target point.
		this.gameObject.transform.rotation = Quaternion.Slerp(this.gameObject.transform.rotation, targetRotation, this.smooth * Time.deltaTime);
		if (this.choice == true) 
		{
			this.__counter += Time.deltaTime;

			if(Input.GetKeyDown(KeyCode.LeftArrow))
			{
				this.input = 1;
				Resolve();
			}
			if(Input.GetKeyDown(KeyCode.RightArrow))
			{
				this.input = 2;
				Resolve();

			}
			if(this.__counter >= this.timeToReact || Input.GetKeyDown(KeyCode.UpArrow))
			{
				this.input = 0;
				Resolve();
			}
		}


		if (this.choice == true) 
		{
			Time.timeScale = 0.1f;
		}

		}





	void Resolve()
	{
		this.targetState += 1;
	
		this.Target[1] = this.challengeScript.gates[input];
		this.__counter = 0;
		this.input = 0;
		this.choice = false;
		Time.timeScale = 1;
		this.Speed = this.Speed * 2;
		if (this.Speed <= this.limitSpeed) 
		{
			this.normalSpeed = this.normalSpeed + this.maxSpeed;
		}
		if(this.timeToReact >= 0.05f)
		{
			this.timeToReact = this.timeToReact-this.timeToReactMalus;

		}
		
	}

	void OnTriggerEnter (Collider other) {
		if (other.gameObject.tag == "challenge") 
		{
			this.choice = true;
			this.challengeScript.gateObject.SetActive(true);


		}



		if (other.gameObject.tag == "S1" || other.gameObject.tag == "S2" || other.gameObject.tag == "S3") 
		{
			this.targetState -= 1;
			
		}

		if(other.tag == "S1" && this.shapeId == 1 || other.tag == "S1" && this.shapeId == 2)
		{
			print ("loose");
		}

		if(other.tag == "S2" && this.shapeId == 0 || other.tag == "S2" && this.shapeId == 2)
		{
			print ("loose");
		}

		if(other.tag == "S3" && this.shapeId == 1 || other.tag == "S3"&& this.shapeId == 0)
		{
			print ("loose");
		}

		if (other.gameObject.tag == "Way") 
		{

			Instantiate(this.pattern[Random.Range(0,this.pattern.Length-1)], this.challengePosition,this.rotateZero);
			this.challengePosition = new Vector3 (this.gameObject.transform.localPosition.x, this.gameObject.transform.localPosition.y, this.gameObject.transform.localPosition.z + popDistance);
			this.Speed = this.normalSpeed;
			this.shapeId = Random.Range (0, this.Shape.Length );
			this.model.GetComponent<MeshFilter>().mesh = this.Shape [this.shapeId];
			this.smooth += 0.5f;
		}


	}

}
