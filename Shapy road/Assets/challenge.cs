﻿using UnityEngine;
using System.Collections;

public class challenge : MonoBehaviour {
	public GameObject target;
	public character characterScript;
	public GameObject characterObject;
	public GameObject gateObject;
	public GameObject[] gates;


	// Use this for initialization
	void Start () {
		this.gateObject.SetActive (false);
		this.characterObject = GameObject.FindGameObjectWithTag ("Character");
		this.characterScript = this .characterObject.GetComponent<character> ();
		this.characterScript.challengeScript = this.gameObject.GetComponent<challenge> ();
		this.characterScript.targetState = 0;
		this.characterScript.Target[0] = this.target;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
